module.exports = function(grunt) {
    grunt.initConfig({
        karma: {
            unit: {
                configFile: 'test/karma.conf.js',
                autoWatch: true
            }
        }
    });



    // Register tasks.
    grunt.loadNpmTasks('grunt-karma');

    // Default task.
    grunt.registerTask('default', 'karma');
};