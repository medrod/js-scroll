    var  scrollList = {
        maxElements : 4,
        listHeight  : 300,
        lastScrollPosition : 0,
        index : 0,
        dataList : [],

        init : function(list, data){
            list.style.height = this.listHeight;
            this.lastScrollPosition = 0;
            this.index = 0;
            this.list = list;
            this.dataList = data;
            this._clearList();
            this._initList();
        },

        _clearList : function(){
            while (this.list.hasChildNodes())
                this.list.removeChild(this.list.lastChild);
        },

        _initList :function(){
            for(var i = 0; i < this.maxElements && this.dataList[i]; i++){
                this.list.appendChild(this._createElement(this.dataList[i]));
            }
        },

        _createElement : function(entry){
            var element = document.createElement('div');
            var elementHeight = this.listHeight / (this.maxElements-1) +'px';

            element.style.height =  elementHeight;
            element.style['line-height'] =  elementHeight;
            element.style.margin = '5px';
            if(entry){
                var img = new Image();
                img.src = entry.img;
                element.appendChild(img);
                var text = document.createElement('div');
                text.innerHTML =  entry.name;
                text.className = 'name';
                element.appendChild(text);
                element.className = 'item';
            }
            return element
        },

        scroll : function ()
        {

            var list         = this.list,
                first        = list.firstElementChild,
                last         = list.lastElementChild,
                top          = list.scrollTop,
                data,
                isGoingUp    = this.lastScrollPosition < top,
                isGoingDown  = this.lastScrollPosition > top;


            if(isGoingUp && top >= first.offsetHeight){
                //add element to the end of the list and remove the first element
                data = this.dataList[this.index + this.maxElements + 1];
                if(data){
                    list.removeChild(first);
                    list.appendChild(this._createElement(data));
                    list.scrollTop = 0;
                    this.index++;
                }
            }else if(isGoingDown && top === 0 ){
                //add element at top of the list and remove the last element
                data = this.dataList[this.index - 1];
                if(data){
                    list.scrollTop = 100;
                    list.removeChild(last);
                    var element = this._createElement(data);
                    list.insertBefore(element, first);
                    this.index --;
                }
            }
            this.lastScrollPosition = list.scrollTop;
        }

    };

    (function(){
        var list = document.getElementById("list");
        if(list){
            //initialize scroll handling
            scrollList.init(list,data);
            list.addEventListener('scroll',function() {
                scrollList.scroll();
            });
        } else{
            console.log('Could not found element list');
        }
    })();


