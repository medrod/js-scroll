var dataProviderHelper = {
    getDataList : function(count){
    var data = [];
    for (var i = 0; i < count; i++) {
        var entry = {id: i,
            name: 'some name ' + i,
            img: 'http://some.image/image' + i + '.jpg'
        };
        data.push(entry);
    }
    return data;
    }

};