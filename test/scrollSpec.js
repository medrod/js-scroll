describe("Scroll List", function() {
    var list;
    var div;
    beforeEach(function(){
        var div = $('<div id="list" class="list" style="height: 500px; overflow: scroll"></div>');
        $(document.body).append(div);
        list =  document.getElementById("list");
        scrollList.maxElements = 6;
        scrollList.listHeight  = 500;
    });

    afterEach(function(){
        $(document.body).chirdren = [];
    });

    it("has to be initialized without data", function() {
        scrollList.init(list,dataProviderHelper.getDataList(0));
        expect($('#list').children().length).toBe(0);
    });

    it("has to be initialized without less than 6 entries", function() {
        scrollList.maxElements = 6;
        scrollList.init(list,dataProviderHelper.getDataList(5));
        expect($('#list').children().length).toBe(5);
    });

    it("has to be initialized without more than 6 entries", function() {
        scrollList.init(list,dataProviderHelper.getDataList(200));
        expect($('#list').children().length).toBe(6);
    });

    it("has to be initialized without less than 3 entries", function() {
        scrollList.maxElements = 3;
        scrollList.init(list,dataProviderHelper.getDataList(200));
        expect($('#list').children().length).toBe(3);
    });

    it("after init first element should be link to img 0", function() {
        scrollList.init(list,dataProviderHelper.getDataList(10));
        var firstImg = list.children[0].children[0];
        expect(firstImg.nodeName).toEqual('IMG');
        expect(firstImg.src).toEqual('http://some.image/image0.jpg');
    });

    it("after init last element should be link to img 5", function() {
        scrollList.init(list,dataProviderHelper.getDataList(10));
        var lastIndex = list.children.length - 1;
        var firstImg = list.children[lastIndex].children[0];
        expect(firstImg.nodeName).toEqual('IMG');
        expect(firstImg.src).toEqual('http://some.image/image5.jpg');
    });


    it("after scroll down 100 px (one entry element height) has to remove the first entry", function() {
        scrollList.init(list,dataProviderHelper.getDataList(10));
        list.scrollTop = 100;
        scrollList.scroll();
        var firstImg = list.children[0].children[0];
        expect(firstImg.src).toEqual('http://some.image/image1.jpg');
    });


    it("after scroll down two times 100 px (two entry element height) has to remove the first two entry", function() {
        scrollList.init(list,dataProviderHelper.getDataList(10));
        list.scrollTop = 100;
        scrollList.scroll();
        var firstImg = list.children[0].children[0];
        expect(firstImg.src).toEqual('http://some.image/image1.jpg');
        list.scrollTop = 100;
        scrollList.scroll();
        firstImg = list.children[0].children[0];
        expect(firstImg.src).toEqual('http://some.image/image2.jpg');
    });

    it("after scroll down and up (each 100px) first list element has to include first entry", function() {
        scrollList.init(list,dataProviderHelper.getDataList(10));
        list.scrollTop = 110;
        scrollList.scroll();
        var firstImg = list.children[0].children[0];
        expect(firstImg.src).toEqual('http://some.image/image1.jpg');
        //the next two steps ar simulate scrolling up
        list.scrollTop = 20;
        scrollList.scroll();
        list.scrollTop = 0;
        scrollList.scroll();
        firstImg = list.children[0].children[0];
        expect(firstImg.src).toEqual('http://some.image/image0.jpg');
    });
});